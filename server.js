var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
var moment = require('moment');
var cors = require('cors');
var schedule = require('node-schedule');

//CORS middleware
// var corsMiddleware = function(req, res, next) {
//     res.header('Access-Control-Allow-Origin', 'localhost'); //replace localhost with actual host
//     res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, PUT, PATCH, POST, DELETE');
//     res.header('Access-Control-Allow-Headers', 'Content-Type, X-Requested-With, Authorization');

//     next();
// }

app.use(cors({credentials: true, origin: true}));

let isPlayerOkList = [false, false, false, false];
let isPlayerSelected = [false, false, false, false];
let isPlayerPut = [false, false, false, false];
let isPlayerScoreDisplay = [false, false, false, false];
let allPlayerStatus = [];
server.lastObjectID = 0;

server.listen(process.env.PORT || 8081, function() {
    console.log('Listening on ' + server.address().port);
});

app.get('/', function(req, res) {
    res.send('Hello World!');
});

app.post('/newPlayer', function(req, res) {
    let assignedNum = -1;
    for (let i=0;i<isPlayerOkList.length;i++) {
        if (!isPlayerOkList[i]) {
            isPlayerOkList[i] = true;
            assignedNum = i+1;
            break;
        }
    }
    res.json({ id: assignedNum });
});

app.post('/checkAvailableRole', function(req, res) {
    const selectedPlayers = [];
    const allPlayer = getAllElements('player');
    for (const player of allPlayer) {
        if (player.role) selectedPlayers.push(player.role);
    }
    res.json({ selectedPlayers });
});

app.post('/getAllPlayers', function(req, res) {
    const allPlayers = getAllElements('player');
    res.json({ allPlayers });
});

// schedule ==> to check player status health
// const job = schedule.scheduleJob('*/5 * * * * *', function(){
//     console.log('Schedule checking');
//     for (const socketID in io.sockets.connected) {
//         const element = io.sockets.connected[socketID].player;
//         if (!element) continue;
//         if (element.type === 'player' && element.updatedTimeUnix < moment().subtract(5, 's').format('x')) {
//             io.emit('statusFailed', { ...element });
//         }
//     }
// });

io.on('connection', function(socket) {
    // Triggered whe new user is in 
    // We can use socket to communicate with clients
    console.log('connected');

    // Add eventlistener to message "newplayer"
    socket.on('initialize', function(newPlayerMeta) {
        /* [Shared] */
        socket.on('close', (d) => {
            isPlayerOkList[d.playerId - 1] = false;
            isPlayerSelected[d.playerId - 1] = false;
            isPlayerPut[d.playerId - 1] = false;
        });
        socket.on('disconnect', () => {
            isPlayerOkList[socket.player.id - 1] = false;
            isPlayerSelected[socket.player.id - 1] = false;
            isPlayerPut[socket.player.id - 1] = false;
        });

        socket.on('closeAll', () => {
            isPlayerOkList = [false, false, false, false];
            isPlayerSelected = [false, false, false, false];
            isPlayerPut = [false, false, false, false];
            stateChange(0);
        });

        const stateChange = (stateId) => {
            const stateMap = {
                0: 'toMenu',
                1: 'toState1',
                2: 'toState2',
                3: 'toState3'
            }
            io.sockets.emit(stateMap[stateId], {});
        };
        /* End [Shared] */
        /* [Menu State] */
        console.log('newPlayer');
        socket.player = {
            id: newPlayerMeta.id,
            type: 'player',
            role: null,
            updatedTime: moment().format(),
            updatedTimeUnix: moment().format('x')
        };

        socket.on('roleDesignated', (data) => {
            socket.player.role = data.role;
            console.log(data.role);
            socket.broadcast.emit('roleSelected', { role: data.role });
            // Check whether all person has selected
            const allPlayer = getAllElements('player');
            let allSet = true; 
            for (const player of allPlayer) {
                if (!player.role) allSet = false;
            }
            if (allSet && allPlayer.length >= 4) stateChange(1);
        });

        // socket.emit is used to trigger the designated function to all connection
        // we can send parameter(the value that getAllPlayers() return) along with message
        socket.emit('allPlayers', getAllElements('player'));

        /* END [Menu State] */
        /* [Play] phase 1 */
        socket.on('selectedObstacle', (d) => {
            isPlayerSelected[d.playerId - 1] = true;
            let isAllSelected = true;
            for (const isSelected of isPlayerSelected) {
                if (!isSelected) isAllSelected = false;
            }
            if (isAllSelected) stateChange(2);

            socket.broadcast.emit('otherObstacleCreate', {
                key: d.key,
                playerId: d.playerId
            })
        });
        /* END [Play] phase 1 */
        /* [Play] phase 2 */
        socket.on('createObstacle', (data) => {
            isPlayerPut[data.playerId - 1] = true;
            socket.broadcast.emit('receiveObstacle', data);

            let isAllSelected = true;
            for (const isSelected of isPlayerPut) {
                if (!isSelected) isAllSelected = false;
            }
            if (isAllSelected) stateChange(3);
        });

        socket.on('obstacleMove', (d) => {
            socket.broadcast.emit('otherObstacleMove', d);
        });
        /* END [Play] phase 2 */

        // socket.broadcast.emit is used to trigger the designated function to all connection except itself
        socket.broadcast.emit('newPlayer', socket.player);

         /* [Play] phase 3 */
        socket.on('move', (data) => {
            // console.log(data.playerId);
            // console.log(socket.player.id);
            // socket.player.frame = data.frame;
            // socket.player.x = data.x;
            // socket.player.y = data.y;

            console.log('move event: %j', data);
            // Only sync the user (client can sync by type)
            socket.broadcast.emit('syncMove', {
                id: data.playerId,
                x: data.x,
                y: data.y,
                frame: data.frame
            });
        });
        socket.on('playerStatusChange', (data) => {
            allPlayerStatus.push(data);

            if (allPlayerStatus.length >= 4) {
                io.sockets.emit('allPlayerStatusSet', allPlayerStatus);
                allPlayerStatus = [];
            }
        });
         /* END [Play] phase 3 */

         /* [Play] phase 4 */
         socket.on('scoreDisplay', (data) => {
            isPlayerScoreDisplay[data.playerId - 1] = true;

            let isAllSelected = true;
            for (const isSelected of isPlayerScoreDisplay) {
                if (!isSelected) isAllSelected = false;
            }
            if (isAllSelected) {
                setTimeout(() => {
                    isPlayerSelected = [false, false, false, false];
                    isPlayerPut = [false, false, false, false];
                    isPlayerScoreDisplay = [false, false, false, false];
                    io.sockets.emit('finishScoreDisplay', {});
                }, 3000);
            }

         });
         /* END [Play] phase 4 */

        socket.on('statusCheck', (data) => {
            Object.keys(io.sockets.connected).forEach((socketID) => {
                const element = io.sockets.connected[socketID].element;
                if (element.id === data.id && element.type === 'player') {
                    element.updatedTime = moment().format();
                    element.updatedTimeUnix = moment().format('x');
                }
            });
        });

        //** Test Flow */
        socket.on('testFlow', (data) => {
            socket.broadcast.emit('testFlowRes', { time: data.time });
        });

        socket.on('disconnect', function() {
            io.emit('remove', socket.player.id);
        });
    });

    socket.on('newObject', (newObject) => {
        console.log('newObject');

        socket.object = {
            id: server.lastObjectID++,
            x: newObject.x,
            y: newObject.y,
            objType: newObject.type,
            type: 'object'
        };

        socket.emit('allObjects', getAllElements('object'));
    });

    socket.on('test', function() {
        console.log('test received');
    });
});

// Get all the players which are already in the game
// in order to let new player can update old players data
function getAllElements(type) {
    const elements = [];
    Object.keys(io.sockets.connected).forEach(function(socketID) {
        const elementRough = io.sockets.connected[socketID];
        const element = elementRough[type];
        if (element && element.type === type) elements.push(element);
    });
    return elements;
}

function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}